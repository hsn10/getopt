package com.filez.scala.getopt

import org.scalatest._
import org.scalatest.funspec._
import org.scalatest.matchers.should._

class ExampleSpec extends AnyFunSpec with Matchers {
   describe("Real world usage example test") {
      it("Invoke Example object extending App containing nonstrict getopt") {
         noException should be thrownBy
         Example.main(Array("one", "-a","two","-b","arg","-d"))
      }
      it("should have two arguments") {
         assert ( Example.arguments === Seq("one", "two") )
      }
      it("should have -a option without arg") {
         assert ( Example.options.get('a') === Some("") )
      }
      it("should have -b option with argument") {
         assert ( Example.options.get('b') === Some("arg") )
      }
      it("should not have -c option") {
         assert ( Example.options.get('c') === None )
      }
      it("should have unknown -d option") {
         assert ( Example.options.get('d') === Some(null))
      }
   }
}