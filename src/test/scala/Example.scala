package com.filez.scala.getopt

object Example {
   protected var g : getopt = _
   def main(args: Array[String]) = {
      g = new getopt(args, "ab:c")
   }
   lazy val arguments = g.arguments
   lazy val options = g.options
}
