/**
 * Copyright (c) Radim Kolar 2013, 2018
 *
 * Licensed under MIT licence:
 *   http://www.opensource.org/licenses/mit-license.php
**/

package com.filez.scala.getopt

import org.scalatest._
import org.scalatest.funspec._

class getoptSpec extends AnyFunSpec with PrivateMethodTester {
   val empty = Seq[String]()
   describe("Invalid optstring variations") {
      it("double ::") {
         intercept[IllegalArgumentException] {
            new getopt(empty, "a::b")
         }
      }
      it("empty") {
         intercept[IllegalArgumentException] {
            new getopt(empty, "")
         }
      }
      it(": at start") {
         intercept[IllegalArgumentException] {
            new getopt(empty, ":pa")
         }
      }
      it("not alphanum") {
         intercept[IllegalArgumentException] {
            new getopt(empty, "@#$%")
         }
      }
      it("spaces outside") {
         intercept[IllegalArgumentException] {
            new getopt(empty, " a:b ")
         }
      }
      it("spaces inside") {
         intercept[IllegalArgumentException] {
            new getopt(empty, "a: b")
         }
      }
      it("spaces only") {
         intercept[IllegalArgumentException] {
            new getopt(empty, "  ")
         }
      }
   }

   describe("optstring parser") {
      it("without args") {
         val g = new getopt(empty, "abc")
         for ( opt <- List('a','b','c') ) {
            assert (g.options_map(opt) === false )
         } 
      }
      it("with args") {
         val g = new getopt(empty, "a:b:c:")
         for ( opt <- List('a','b','c') ) {
            assert (g.options_map(opt) === true )
         }
      }
      it("mixed args") {
         val g = new getopt(empty, "ab:c")
         for ( opt <- List('a','c') ) {
            assert (g.options_map(opt) === false )
         }
         assert (g.options_map('b') === true )
      }
   }

   describe("parseElement function") {
      val gopt = new getopt(empty, "a:b")
      it("throws IllegalArgument on zero sized input") {
         intercept[IllegalArgumentException] {
            gopt.parseElement("")
         }
      }
      it("throws NoSuchElement on -- input") {
         intercept[NoSuchElementException] {
            gopt.parseElement("--")
         }
      }
      it("Non option element") {
         val (a1, a2, a3) = gopt.parseElement("karel")
         assert (a1.size === 0)
         assert (a2 === "karel")
         assert (a3 === null)
      }
      it("option without argument") {
         val (a1, a2, a3) = gopt.parseElement("-b")
         assert ( a1.get('b') === Some("") )
         assert ( a2 === null )
         assert ( a3 === null )
      }
      it("unknown option") {
         val (a1, a2, a3) = gopt.parseElement("-Z")
         assert ( a1.get('Z') === Some(null) )
         assert ( a2 === null )
         assert ( a3 === null )
      }
      it("option with argument") {
         val (a1, a2, a3) = gopt.parseElement("-amazing")
         assert ( a1.get('a') === Some("mazing") )
         assert ( a2 === null )
         assert ( a3 === null )
      }
      it("option with in next element") {
         val (a1, a2, a3) = gopt.parseElement("-a")
         assert ( a1.size === 0 )
         assert ( a2 === null )
         assert ( a3 === 'a' )
      }
   }

   describe("basic getopt parsing") {
      it("no options") {
        val g = new getopt(Seq("one","two"), "a")
        assert ( g.arguments.size === 2 )
        assert ( g.options.size === 0 )
      }
      it("one option with arg alone") {
        val g = new getopt(Seq("one","-a", "two"), "a:")
        assert ( g.options.get('a') === Some("two") )
        assert ( g.arguments === Seq("one") )
      }
      it("two options alone without arg") {
        val g = new getopt(Seq("one", "-a", "-b", "two"), "ab")
        assert ( g.options.get('a') === Some("") )
        assert ( g.options.get('b') === Some("") )
        assert ( g.arguments === Seq("one","two") )
      }
      it("option with missing argument") {
        val g = new getopt(Seq("one","two","-c"), "c:")
        assert ( g.arguments === Seq("one","two") )
        assert ( g.options.get('c') === Some(null) )
      }
      it("option with missing argument in strict mode") {
        intercept[NoSuchElementException] {
            new getopt(Seq("one","two","-c"), "c:", true)
        }
      }
   }
   describe("advanced option parsing") {
      it("one option with arg in the same element") {
        val g = new getopt(Seq("one","-atwo","three"), "a:")
        assert ( g.options.get('a') === Some("two") )
        assert ( g.arguments === Seq("one","three") )
      }
      it("two options together without arg") {
        val g = new getopt(Seq("one","-ab", "two"), "ab")
        assert ( g.options.get('a') === Some("") )
        assert ( g.options.get('b') === Some("") )
        assert ( g.arguments === Seq("one","two") )
      }
      it("dash without option in strict mode") {
        val g = new getopt(Seq("one","-", "two"), "ab", true)
        assert ( g.arguments === Seq("one", "-", "two") )
      }
      it("two options with arg in same element") {
        val g = new getopt(Seq("one","-abtwo", "three"), "ab:", true)
        assert ( g.arguments === Seq("one","three") )
        assert ( g.options.get('a') === Some("") )
        assert ( g.options.get('b') === Some("two") )
      }
   }

   describe("option validate() function") {
      it("unknown option without argument") {
        val g = new getopt(Seq("one","two","-c", "-a"), "a")
        intercept[NoSuchElementException] {
            g.validate()
         }
      }
      it("unknown option with argument") {
        val g = new getopt(Seq("one","two","-c", "vole"), "a")
        intercept[NoSuchElementException] {
            g.validate()
         }
      }
      it("required option argument missing") {
        val g = new getopt(Seq("one","two","-c"), "c:")
        intercept[NoSuchElementException] {
            g.validate()
         }
      }
      it("no options on command line but options defined") {
        val g = new getopt(Seq("one","two","three"), "abc:")
        g.validate()
        assert ( g.arguments.size === 3 )
      }
   }

   describe("options with GNU extension --") {
      it("no options -- in middle") {
        val g = new getopt(Seq("one","--","two"), "a")
        assert ( g.arguments === Seq("one","two") )
      }
      it("no options with -- at end") {
        val g = new getopt(Seq("one","two","--"), "a")
        assert ( g.arguments === Seq("one","two") )
      }
      it("options with -- in midle") {
        val g = new getopt(Seq("one","two","--", "-a"), "a")
        assert ( g.arguments === Seq("one","two","-a" ) )
      }
      it("options after --") {
        val g = new getopt(Seq("--","-a2"), "a:")
        assert ( g.arguments === Seq("-a2" ) )
      }
      it("-- following option with argument") {
        val g = new getopt(Seq("one","two","-c", "--", "end"), "c:")
        assert ( g.arguments === Seq("one","two", "end") )
        assert ( g.options.get('c') === Some("--") )
      }
   }

   describe("non strict unknown option parsing") {
      it("unknown option without argument") {
        val g = new getopt(Seq("one","two","-c", "-a"), "a")
        assert ( g.arguments === Seq("one","two") )
        assert ( g.options.get('c') === Some(null) )
        assert ( g.options.get('a') === Some("") )
      }
      it("unknown option with argument") {
        val g = new getopt(Seq("one","two","-c", "vole"), "a")
        assert ( g.arguments === Seq("one","two", "vole") )
        assert ( g.options.get('c') === Some(null) )
      }
   }
}