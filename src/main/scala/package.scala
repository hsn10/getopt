package com.filez.scala

/**
  POSIX getopt(3) command line argument parser with GNU '--' extension.
  @author Radim Kolar
  @version 1.1
*/
package object getopt {
}