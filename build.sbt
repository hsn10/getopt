val sharedSettings = Seq(

name := "POSIX getopt for Scala",

normalizedName := "getopt",

organization := "com.filez.scala",

organizationName := "Filez.com",

startYear := Some(2013),

version := "1.2.4",

crossScalaVersions := Seq("2.12.17", "2.11.12", "2.13.10", "3.1.3"),

ThisBuild / scalaVersion := crossScalaVersions.value.head,

Test / parallelExecution := false,

ThisBuild / versionScheme := Some("semver-spec")
)

val publishSettings = Seq(

Test / publishArtifact := true,

publishTo := sonatypePublishToBundle.value,

sonatypeProfileName := "com.filez",

Global / useGpg := false,

pomExtra := (
  <url>https://gitlab.com/hsn10/getopt</url>
  <licenses>
    <license>
      <name>MIT</name>
      <url>http://www.opensource.org/licenses/mit-license.php</url>
      <distribution>repo</distribution>
    </license>
  </licenses>
  <scm>
    <connection>scm:git:https://gitlab.com/hsn10/getopt.git</connection>
    <developerConnection>scm:git:git@gitlab.com:hsn10/getopt.git</developerConnection>
    <url>git@gitlab.com:hsn10/getopt.git</url>
  </scm>
  <developers>
    <developer>
      <id>hsn10</id>
      <name>Radim Kolar</name>
    </developer>
  </developers>)
)

lazy val root = (project in file("."))
  .aggregate(getopt.js, getopt.jvm)
  .settings( Compile / skip := true )
  .settings( publish / skip := true )
  .settings( doc     / skip := true )
  .settings( publishSettings )

lazy val getopt =
  crossProject(JVMPlatform, JSPlatform)
  .crossType(CrossType.Pure)
  .in(file("."))
  .settings(sharedSettings)
  .settings(publishSettings)
  .settings(
    libraryDependencies += "org.scalatest" %%% "scalatest" % "3.2.14" % "test"
  ).jvmSettings(Seq(
      scalacOptions ++= Seq(
						"-deprecation",		// Emit warning and location for usages of deprecated APIs.
						"-feature",		// Emit warning and location for usages of features that should be imported explicitly.
						"-unchecked",		// Enable additional warnings where generated code depends on assumptions.
						"-explaintypes"		// Explain type errors in more detail.
      ),
      Compile / doc / scalacOptions := Seq("-groups", /* "-implicits", */"-no-link-warnings")
  )).jsSettings(Seq(
    jsEnv := new org.scalajs.jsenv.nodejs.NodeJSEnv(),
    Test / scalaJSLinkerConfig ~= { _.withOptimizer(false) }
  ))
