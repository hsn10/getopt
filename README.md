POSIX getopt(3) for Scala 2 / 3 / JS
====================================
*Version 1.2.4* [MIT Licensed](https://opensource.org/licenses/MIT)

Supports Scala 2.11, 2.12, 2.13, 3.1 [![Scala.js](https://www.scala-js.org/assets/badges/scalajs-1.0.0.svg)](https://www.scala-js.org)
[![pipeline status](https://gitlab.com/hsn10/getopt/badges/master/pipeline.svg)](https://gitlab.com/hsn10/getopt/commits/master)
[![](https://tokei.rs/b1/gitlab/hsn10/getopt?category=code)](https://github.com/XAMPPRocky/tokei)

#### Old Scala versions

 * If you need ScalaJS 0.6 or Scala 2.10 then use getopt version 1.1.0
 * If you need Scala 3.0 then use geopt version 1.2.2

Features
--------

1. GNU argument parsing rules. Options can be anywhere in command line before --
1. GNU -- extension. Everything after -- is not treated as options
1. Multiple options not requiring argument can be grouped together.
   -abc is the same as -a -b -c
1. Argument does not require space. -wfile is same as -w file

Usage
-----

#### Import

  *import* **com.filez.scala.getopt.getopt**

#### Create getopt instance

  val g = *new* **getopt**(_arguments_, _optstring_, _strict_)

  _arguments_ command line arguments as Seq[String]

  _optstring_ is a String containing the legitimate option characters.
       If such a character is followed by a colon, the option requires an
       argument.

  _strict_ If strict mode is enabled then unknown exceptions or missing arguments
           raise an *NoSuchElementException.*

#### Check for results

  *getopt* instance has following properties:
  1. _arguments_ : Seq[String] command line arguments with options removed
  2. _options_map_ : Map[Char, Boolean] map of recognized options. option -> have_argument
  3. _options_ : Map[Char, String] options parsed. If option do not have argument, it is mapped to "" string. If
     option is detected but unknown it is mapped to *null*, otherwise it is mapped to its
     argument as string.

#### Optional - Check if options are parsed correctly
  If you did not created *getopt* instance in strict mode, you can run strictness check
  by calling *validate()* instance function.

#### Example usage

```scala
  import com.filez.scala.getopt.getopt

  object Example extends App {
     protected val g = new getopt(args, "ab:c")
     g.options.get('b').foreach( arg => println(s"-b arg value is $arg") )
  }
```

SBT
---
    libraryDependencies += "com.filez.scala" %% "getopt" % "1.2.4"

BUILDING RELEASE and publishing to Maven Central
------------------------------------------------
    sbt +compile
    sbt +test
    sbt +doc
    sbt +publishSigned
    sbt sonatypePrepare
    sbt +sonatypeBundleUpload
    go to https://oss.sonatype.org/#stagingRepositories and review what got uploaded
    happy? sbt sonatypeRelease

